package org.zcs.spike.server.spark.testing.java.data_preparation;

import com.google.common.collect.Lists;
import org.apache.spark.api.java.JavaRDD;
import org.junit.Test;
import org.zcs.spike.server.spark.testing.java.context.manual.SparkIntegrationTestBase;
import org.zcs.spike.server.spark.testing.java.model.Apple;

import java.util.List;

public class RDDCreationTest extends SparkIntegrationTestBase {
    @Test
    public void testEmptyRDD() {
        jsc().emptyRDD();
    }

    @Test
    public void testPrimitiveList() {
        List<String> data = Lists.newArrayList("green", "red");
        jsc().parallelize(data);
    }

    @Test
    public void testListOfEntities() {
        List<Apple> rows = Lists.newArrayList(new Apple("green", 70), new Apple("red", 110));
        JavaRDD<Apple> result = jsc().parallelize(rows);
        result.foreach(v -> System.out.println(v));
    }


}
