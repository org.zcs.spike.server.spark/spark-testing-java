package org.zcs.spike.server.spark.testing.java.data_preparation;

import com.google.common.collect.Lists;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.junit.Test;
import org.zcs.spike.server.spark.testing.java.context.manual.SparkIntegrationTestBase;
import org.zcs.spike.server.spark.testing.java.model.Apple;

import java.util.List;

public class DataSetCreationTest extends SparkIntegrationTestBase {
    @Test
    public void testListOfEntities() {
        List<Apple> rows = Lists.newArrayList(new Apple("green", 70), new Apple("red", 110));
        final Dataset<Apple> actual = spark().createDataset(rows, Encoders.bean(Apple.class));
        actual.show(false);
    }

}
