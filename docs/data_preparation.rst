==========================================
Input data preparation
==========================================
Several approaches can be used.

**Dynamically from:**

* empty with predefined structure
* one primitive
* primitive list
* tuple list
* data objects list

**Pre saved on disk.** For check reading and for complex structures data, in formats:

* parquet
* csv

**Pre saved and dynamically modified on fly.**
For improve visibility on complex structures testing.
