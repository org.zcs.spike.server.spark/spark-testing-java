==========================================
Framework: spark-testing-base
==========================================

Framework "spark-testing-base" located in `github <https://github.com/holdenk/spark-testing-base>`_

**Drawbacks:** current version provided SparkContext and JavaSparkContext only, some additional code required for get SparkSession.

Inclusion in project with Maven
"""""""""""""""""""""""""""""""

.. code-block:: java

        <dependency>
            <groupId>com.holdenkarau</groupId>
            <artifactId>spark-testing-base_2.11</artifactId>
            <version>2.3.1_0.10.0</version>
            <scope>test</scope>
        </dependency>

Example of `pom.xml <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-java/blob/master/pom.xml>`_

Enabling in testcases
"""""""""""""""""""""""""""""""""""""

Test classes have to extends "SharedJavaSparkContext".

.. code-block:: java

            public class ApplesRepositoryTestingBaseIT extends SharedJavaSparkContext

"SparkContext" and "JavaSparkContext"
"""""""""""""""""""""""""""""""""""""
Can be received with methods: "sc()" "jsc()"

.. code-block:: java
        :emphasize-lines: 3

        Integer[] weights = {120, 150};
        long expected = Arrays.stream(weights).reduce(0, (a, v) -> a + v);
        Dataset<Row> df = new SQLContext(jsc()).createDataset(Arrays.asList(weights), Encoders.INT()).toDF("weight");

        long actual = repository.mass(df);

        assertEquals(expected, actual);

SQLContext creation
"""""""""""""""""""

.. code-block:: java

    new SQLContext(jsc())

Code example: `ApplesRepositoryTestingBaseIT.java <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-java/blob/master/src/test/java/org/zcs/spike/server/spark/testing/java/context/spark_testing_base/ApplesRepositoryTestingBaseIT.java>`_