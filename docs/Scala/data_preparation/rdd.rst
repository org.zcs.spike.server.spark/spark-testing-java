==========================================
RDD
==========================================

Empty RDD with predefined schema
"""""""""""""""""""""""""""""""""""""

.. code-block:: scala

        val rdd = sparkContext.emptyRDD[Apple]


List of primitives
"""""""""""""""""""""""""""""""""""""

.. code-block:: scala

        val data = List(1, 2, 3)
        val rdd = sparkContext.parallelize(data)


List of entities
"""""""""""""""""""""""""""""""""""""

.. code-block:: scala

        val expected = Apple("Green", 120)
        val rdd = sparkContext.parallelize(List(expected))

Example: `RDDCreationSpec.scala <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-scala/blob/master/src/test/scala/org/zcs/spike/server/spark/testing/scala/data_preparation/RDDCreationSpec.scala>`_