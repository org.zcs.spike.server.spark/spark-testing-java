==========================================
Dataset
==========================================

Empty
"""""""""""""""""""""""""""""""""""""

.. code-block:: scala

        val df = Seq.empty[Apple].toDS()


From list
"""""""""""""""""""""""""""""""""""""

.. code-block:: scala

        List(Apple("green", 70), Apple("red", 110)).toDS()


From DataFrame
"""""""""""""""""""""""""""""""""""""

.. code-block:: scala

        List(("green", 70)).toDF("color", "weight")
          .as[Apple]

Example: `DataSetCreationSpec.scala <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-scala/blob/master/src/test/scala/org/zcs/spike/server/spark/testing/scala/data_preparation/DataSetCreationSpec.scala>`_
