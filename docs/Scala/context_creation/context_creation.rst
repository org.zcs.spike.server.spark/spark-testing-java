==========================================
Context creation
==========================================
Library from Spark distributive is the best choice as base for integration testing, here named as "spark-test-jar".
Code examples in package: `context <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-scala/tree/master/src/test/scala/org/zcs/spike/server/spark/testing/scala/context>`_

.. toctree::
    :hidden:

    manual.rst
    spark-test-jar.rst
