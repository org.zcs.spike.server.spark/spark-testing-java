==========================================
Manual
==========================================
Two cores (highlighted) used in this example.

.. code-block:: scala
        :emphasize-lines: 4

        val spark = SparkSession
          .builder
          .appName(getClass().getSimpleName)
          .master("local[2]")
          .getOrCreate()

        // use it

        spark.close()

Code example: `ManualSpec.scala <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-scala/blob/master/src/test/scala/org/zcs/spike/server/spark/testing/scala/context/ManualSpec.scala>`_